var express = require('express'); //ES2015 is import
var bodyParser = require('body-parser');
var path = require('path');
var expressValidator = require('express-validator');
var mongojs = require('mongojs');
var db = mongojs('customerapp', ['users']);
var app = express();

/*
//Example Middleware
//order of this middleware is important
//must go before route handler
var logger = function(req, res, next) {
    console.log('Logging..');
    next();
}
*/

//Add view engine middleware
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));


//Body Parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Set Static Path middleware
app.use(express.static(path.join(__dirname, 'public')));

//Global Vars
app.use(function(req, res, next) {
    res.locals.errors = null;
    next();
})

//Use express-validator middleware
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;
        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };

    }
}));

/*now coming from the db
var users = [{
        id: 1,
        first_name: 'John',
        last_name: 'Doe',
        email: 'johndoe@gmail.com'
    },
    {
        id: 2,
        first_name: 'Bob',
        last_name: 'Smith',
        email: 'bobsmith@gmail.com'
    },
    {
        id: 3,
        first_name: 'Jill',
        last_name: 'Jackson',
        email: 'jilljackson@gmail.com'
    }
];*/

var person = {
    name: 'Jeff',
    age: 30
};

var persons = [{
        name: 'Jeff',
        age: 30
    },
    {
        name: 'Sara',
        age: 22
    },
    {
        name: 'Bill',
        age: 40
    }
];

//home route handler
app.get('/', function(req, res) {
    //res.send('Hello');
    //res.json(person);
    //res.json(persons);

    //docs is an array of all the documents in users
    db.users.find(function(err, docs) {
        console.log(docs);

        res.render('index', {
            title: 'Customers',
            users: docs
        });
    });
});

app.post('/users/add', function(req, res) {
    //console.log('FORM SUBMITTED');
    ////gets the first name from the post
    //console.log(req.body.first_name);

    //using express-validator
    //checks that first name, last name and email is required
    req.checkBody('first_name', 'First name is required').notEmpty();
    req.checkBody('last_name', 'Last name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.render('index', {
            title: 'Customers',
            users: db.users.find(),
            errors: errors
        });
        //console.log('Errors');
    } else {
        var newUser = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email
        };
        //console.log(newUser);
        //console.log('Success');
        db.users.insert(newUser, function(err, res) {
            if (err) {
                console.log(err);
            } else {
                res.redirect('/');
            }
        });
    }
});

app.listen(3001, function() {
    console.log('Server started on Port 3001');
});